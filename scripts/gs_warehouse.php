<?php
// require __DIR__ . '/vendor/autoload.php';
// include(__DIR__.'/vendor/autoload.php'); 


function findRow($name){
    $client = new \Google_Client();
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig(__DIR__.'/credentials.json');
    $service = new Google_Service_Sheets($client);

    $spreadsheetId = "1_agep7clDCft8vgi_of-G2buupMfR9V--oOdfodQWQY"; 
    $get_range = "на складе!A:Z";
    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        if($value[1]===$name){
            $index = $key+1;
            $quantity = $value[2];
            print_r($index.' index of finded row');
            break;
        }else{
            $index = false;
            $quantity = 1;
        }
    }

   
    echo PHP_EOL;
    
    return [$index,$quantity];
}

function updateCell($index , $quantity){
    $client = new \Google_Client();
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig(__DIR__.'/credentials.json');
    $service = new Google_Service_Sheets($client);

    $spreadsheetId = "1_agep7clDCft8vgi_of-G2buupMfR9V--oOdfodQWQY"; 
    $get_range = "на складе!A:Z";
    $response = $service->spreadsheets_values->get($spreadsheetId, $get_range);
    $values = $response->getValues();
    $sum = '=C'.$index.'*D'.$index;

    $values = [[
        $index-1,
    ],];
    $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
    $params = ['valueInputOption' => 'USER_ENTERED'];
    $upd_range = 'на складе!A'.$index.':B'.$index;
    $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
    $values = [[
        $quantity,
    ],];
    $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
    $params = ['valueInputOption' => 'USER_ENTERED'];
    $upd_range = 'на складе!C'.$index.':D'.$index;
    $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
    
    $values = [[
        $sum,
    ],];
    $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
    $params = ['valueInputOption' => 'USER_ENTERED'];
    $upd_range = 'на складе!E'.$index.':F'.$index;
    $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);
}

function addCell($name, $quantity){
    $client = new \Google_Client();
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig(__DIR__.'/credentials.json');
    $service = new Google_Service_Sheets($client);

    $spreadsheetId = "1_agep7clDCft8vgi_of-G2buupMfR9V--oOdfodQWQY"; 
    $warehouse_range = "на складе!A:Z";
    $prices_range = "Себестоимость товаров!A:Z";
    
    $response = $service->spreadsheets_values->get($spreadsheetId, $prices_range);
    $values = $response->getValues();

    foreach($values as $key=>$value) {
        if($value[2]===$name){
            $price = $value[9];
            break;
        }else{
            $price = '?';
        }
    }

    $response = $service->spreadsheets_values->get($spreadsheetId, $warehouse_range);
    $values = $response->getValues();
    $index = count($values)+1;

    $sum = '=C'.$index.'*D'.$index;
    $values = [[
        $index-1,$name,$quantity,$price,$sum
    ],];
    $body = new Google_Service_Sheets_ValueRange(['values' => $values]);
    $params = ['valueInputOption' => 'USER_ENTERED'];
    $upd_range = 'на складе!A'.$index.':E'.$index;
    $result = $service->spreadsheets_values->update($spreadsheetId, $upd_range, $body, $params);

}

function addQuantity($name,$q){
    $res =  findRow($name);
    $index = $res[0];
    $quantity = $res[1];

    $plus = intval($quantity)+$q;
    if($index!=false){  
        updateCell($index,$plus);
        print_r(' updated cell in add'.$index);
        echo PHP_EOL;
    }else{
        addCell($name,$q.'');
        print_r('added new row in add');
        echo PHP_EOL;
    }
}
function subtrQuantity($name,$q){
    $res =  findRow($name);
    $index = $res[0];
    $quantity = $res[1];

    $plus = intval($quantity)-$q;
    if($index!=false){  
        updateCell($index,$plus);
        print_r('updated cell in add'.$index);
        echo PHP_EOL;
    }else{
        addCell($name,-$q.'');
        print_r('added new row in add');
        echo PHP_EOL;
    }
}

?>