<?php
// print_r('checking COMPLETED');
// echo PHP_EOL;

$q = "SELECT * FROM users";
$users = mysqli_query($link, $q);

$curl = curl_init();
$headers = array(
    'Content-Type: application/json',
    'X-Auth-Token:'.$kaspi_token
);

$d = time()-$timeDifference;
$date = $d*1000;

$url = 'https://kaspi.kz/shop/api/v2/orders?page[number]=0&page[size]=1000&filter[orders][state]=ARCHIVE&filter[orders][status]=COMPLETED&filter[orders][creationDate][$ge]='.$date;
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

$out = curl_exec($curl);
curl_close($curl);
$res = json_decode($out);
$orders = $res->data;

foreach ($orders as &$order) {
    // print_r($order);
    $q = "SELECT * FROM orders WHERE order_id = ".$order->attributes->code;
    $result = mysqli_query($link, $q);
    $row = mysqli_fetch_assoc($result);

    if(isset($row)){
        if($row["step"] === 'PICKUP' || $row["step"] === 'KASPI_DELIVERY_TRANSMITTED'|| $row["step"] === 'DELIVERY'){
            print_r('ORDER IS COMPLETED');
            echo PHP_EOL;

            $state = $order->attributes->state;
            $status = $order->attributes->status;
            $order_id = $row["order_id"];

            //update in mysql db
            $q = "UPDATE orders SET state = '$state', status = '$status', step='COMPLETED' WHERE order_id = '$order_id'";
            $result = mysqli_query($link, $q);

            $product_name = $row["product_name"];
            $order_id = $row["order_id"];
            $cust_fname = $row["cust_fname"];
            $cust_lname = $row["cust_lname"];
            $address = $row["address"];
            $cust_phone = $row["cust_phone"];
            $total_price = $row["total_price"];
            $url = $row["url"];
            $quantity = $row["quantity"];

            print_r($order->attributes->code.' order updated (COMPLETED)');
            echo PHP_EOL;

            //change excel
            updateRowComp($order->attributes->code);

            //send to tg
            foreach ($users as $user) {
                try{
                    $reply = "🔵 <b>ЗАКАЗ ВЫПОЛНЕН</b>".
                    "\n<b>Товар: </b> <i>$product_name</i>".
                    "\n<b>ID заказа: </b> <i>$order_id</i>".
                    "\n<b>Имя: </b> <i>$cust_fname $cust_lname</i>".
                    "\n<b>Адрес: </b> <i>$address</i>".
                    "\n<b>Телефон: </b> <i> +7$cust_phone</i>".
                    "\n<b>Стоимость: </b> <i>$total_price тг.</i>".
                    "\n<b>Количество: </b> <i>$quantity</i>".
                    "\n<b>Ссылка: </b> <i> $url</i>";
                    $telegram->sendMessage([ 'chat_id' => $user['chat_id'], 'text' => $reply ,'parse_mode'=>'HTML']);
                }catch(Exception $e) {
                    //delet user if he has blocked the bot
                    if($e->getMessage()==='Forbidden: bot was blocked by the user'){
                        print('delete this user: '.$user['chat_id']);
                        //update in mysql db
                        $chat_id = $user['chat_id'];

                        $q = "DELETE FROM users WHERE chat_id = '$chat_id'";
                        $result = mysqli_query($link, $q);
                    }
                }
            }

        }
    }
}

    
?>
