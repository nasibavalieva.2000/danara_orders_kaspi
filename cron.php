<?php
include 'vendor/autoload.php'; 
include 'scripts/constants.php';
include 'scripts/connect_to_db.php';//подключение к базе данных (MySQL)
include 'scripts/connect_to_tg.php';//подключение к боту
include 'bot.php';//телеграм бот
include 'scripts/gs_actions.php';//для движения денег
include 'scripts/gs_warehouse.php';//склад
include 'scripts/checkNew.php';//новые заказы 
include 'scripts/checkSignRequired.php';//заказы на подписании в банке
include 'scripts/checkPickup.php';//заказы на самовывозе
include 'scripts/checkDelivery.php';//заказы на доставке по городу
include 'scripts/checkKaspiDelivery.php';//заказы в другие города
include 'scripts/checkTransmitted.php';//заказы которые отправили в другие города
include 'scripts/checkCompleted.php';//выданные заказы
include 'scripts/checkCancelled.php';//отмененные заказы(1.отменены сразу после поступления, 2.отменены после подтверждения и 3.отменены после отправки в другой город, но без выдачи)
include 'scripts/checkReturn.php';//заказы которые были выданы в других городах, но сейчас возвращаются(за ними можно мледить прямо из кабинета)
include 'scripts/checkReturnRequested.php';//заказы которые были выданы в других городах, но сейчас возвращаются(за ними можно мледить прямо из кабинета)
include 'scripts/checkReturned.php';//заказы которые вернулись
?>